<?php

function create_posttype()
{

    register_post_type('Gallery',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Gallery'),
                'singular_name' => __('Gallery')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'gallery'),
            'menu_icon' => 'dashicons-images-alt2',
            'menu_position' => 2,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields','page-attributes'),

        )
    );

    register_post_type('Packages',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Packages'),
                'singular_name' => __('Package')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'packages'),
            'menu_icon' => 'dashicons-money',
            'menu_position' => 2,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields','page-attributes'),

        )
    );

    register_post_type('Exclusive',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Exclusive'),
                'singular_name' => __('Exclusive')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'exclusive'),
            'menu_icon' => 'dashicons-lock',
            'menu_position' => 2,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields','page-attributes'),

        )
    );

}

// Hooking up our function to theme setup
add_action('init', 'create_posttype');