### Changelog

#### **1.0.2** – 28 April 2020

* Improved cross-browser compatibility

--

#### **1.0.1** – 28 March 2020

+ Added Word break settings for Header and Subheader

--

#### **1.0.0** – 14 February 2020

+ Initial release