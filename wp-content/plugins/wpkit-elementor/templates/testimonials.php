<?php
/*
 * Testimonials Template
 * @package WPKit For Elementor
 */

$wke_testimonials      =   $wke_data->testimonials;
$wke_extra_class  	   =   $wke_data->extra_class;
$wke_text_color        =   $wke_data->text_color;

/* Element Frontend */
$id = WKE_Util::random_string( 10, false );
$total_number = 0;
$inline_css = '';

if( $wke_testimonials ){

  if( $wke_text_color !== '' && $wke_text_color !== '#000000' ) {
      $inline_css = 'style="color:' . $wke_text_color . '"';
  }

	$renderHTML = '<div id="wke-testimonials-' . esc_attr( $id ) . '" ' . $inline_css . ' class="wke-testimonials wke-swiper-slider swiper-container swiper-container-horizontal ' . esc_html( $wke_extra_class ) . '" data-direction="horizontal">
					<div class="swiper-wrapper">';

	foreach( $wke_testimonials as $item ){
		$total_number .= $total_number + 1;
		$renderHTML .= '<div class="swiper-slide">';
		  $renderHTML .= '<div class="testimonial-text">
		                      ' . esc_attr( $item['said'] ) . '
		                  </div>

		                  <footer class="testimonial-author">
		                    <img src="' . esc_url( $item['avatar']['url'] ) . '" alt="' . esc_html( $item['name'] ) . '" class="avatar" />
                        <span class="author">' . esc_html( $item['name'] ) .' <em>'.esc_html( $item['job'] ) . '</em></span>
		                  </footer>';
		$renderHTML .= '</div>';
    }

    	$renderHTML .= '</div>';
    	    if( $total_number > 1 ){
				$renderHTML .= '	
					<!-- If we need pagination -->
           		    <div class="swiper-pagination"></div>

					<!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>';
            }
    $renderHTML .= '</div>';

   
}
echo $renderHTML;