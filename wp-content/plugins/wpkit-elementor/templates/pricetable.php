<?php
/*
 * Price Table Shortcode Template
 * @package WPKit For Elementor
 */

$table_name     			 =   $wke_data->name;
$price          			 =   $wke_data->price;
$button_text           =   $wke_data->button_text;
$button_link           =   $wke_data->button_link;
$content         			 =   $wke_data->content;
$featured				       =   $wke_data->featured;
$extra_class    			 =   $wke_data->extra_class;

$allow_HTML_tags = array(
    //formatting
    'strong' => array(),
    'em'     => array(),
    'b'      => array(),
    'i'      => array(),
    'ul'	 => array(),
    'li'	 => array(),
    'ol'	 => array(),
    'span'	 => array(),
    'blockquote' => array(),

    //links
    'a'     => array(
        'href' => array()
    )
);


$renderHTML='<div class="wke-price-table ' . esc_html( $featured ) . ' ' . esc_html( $extra_class ) . '">
   <header>  
     <h3>' . esc_html( $table_name ) . '</h3>';
     if( $price !== '' ) {  
        $renderHTML .= '<div class="price">' . esc_html( $price ) . '</div>';
     }
   $renderHTML .= '</header>
   <div class="price_content">' . do_shortcode( wp_kses( $content,$allow_HTML_tags ) ) . '</div>';
   
   if( $button_link['url'] !== '' ) {
   
     $target = $button_link['is_external'] ? 'target="_blank"' : '';
     
     $renderHTML.='<footer>
           <a href="' . esc_url($button_link['url']) . '" ' . $target . ' class="button wke_price_table_button">' . esc_html( $button_text ) . '</a>
        </footer>';
   }
   
   $renderHTML.='</div>';

echo $renderHTML;