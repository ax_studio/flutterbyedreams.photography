<?php
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\utils;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WKE_PriceTable_Widget extends Widget_Base {

    public function get_name() {
        return 'pricetable';
    }

    public function get_title() {
        return esc_html__('Price Table', 'wpkit-elementor');
    }

    public function get_icon() {
        return 'eicon-price-table';
    }

    public function get_categories() {
        return array('wpkit-common-widget');
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_pricetable_content',
            [
                'label' => esc_html__('Price Table Content', 'wpkit-elementor'),
                'tab'   => Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(

            'name',
            [
                'type' => Controls_Manager::TEXT,
                'label' => esc_html__('Table Name', 'wpkit-elementor')
            ]
        );

        $this->add_control(

            'price',
            [
                'type' => Controls_Manager::TEXT,
                'label' => esc_html__('Price', 'wpkit-elementor'),
                'description' => esc_html__('Do not miss the currency symbol such as $10 or $10/month.', 'wpkit-elementor'),
            ]
        );

        $this->add_control(

            'content',
            [
                'type' => Controls_Manager::TEXTAREA,
                'label' => esc_html__('Table Content', 'wpkit-elementor'),
                'description' => esc_html__('Support shortcode and specific HTML tags: strong, span, ol, ul, li, em, b, i, a, blockquote. ', 'wpkit-elementor'),
            ]
        );

        $this->add_control(

            'button_text',
            [
                'type' => Controls_Manager::TEXT,
                'label' => esc_html__('Button Text', 'wpkit-elementor')
            ]
        );

        $this->add_control(

            'button_link',
            [
                'type' => Controls_Manager::URL,
                'label' => esc_html__('Button Link', 'wpkit-elementor'),
                'description' => esc_html__('If you leave it empty, the button will not be shown.', 'wpkit-elementor'),
            ]
        );


        $this->add_control(
            'featured',
            [
                'label' => esc_html__( 'Mark it As Featured Plan?', 'wpkit-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => esc_html__( 'Yes', 'wpkit-elementor' ),
                'label_off' => esc_html__( 'No', 'wpkit-elementor' ),
                'return_value' => 'featured',
            ]
        );

        $this->add_control(

            'extra_class',
            [
                'type' => Controls_Manager::TEXT,
                'label' => esc_html__('Extra Class', 'wpkit-elementor'),
                'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'wpkit-elementor'),
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        WKE_Extend_Elementor::widget_template(self::get_name(),$this->get_settings());
    }

    protected function _content_template() {
        
    }

}
