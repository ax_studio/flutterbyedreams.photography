<?php
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\utils;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WKE_Testimonials_Widget extends Widget_Base {

    public function get_name() {
        return 'testimonials';
    }

    public function get_title() {
        return esc_html__('Testimonials', 'wpkit-elementor');
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return array('wpkit-common-widget');
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_testimonials',
            [
                'label' => esc_html__('Testimonials', 'wpkit-elementor'),
            ]
        );

        $this->add_control(
            'testimonials',
            [
                'label' => esc_html__( 'Testimonials', 'wpkit-elementor' ),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [
                        'said' => esc_html__( 'Item #1 content', 'wpkit-elementor' ),
                    ]
                ],
                'fields' => [
                    [
                        'name' => 'name',
                        'label' => esc_html__( 'Customer Name', 'wpkit-elementor' ),
                        'type' => Controls_Manager::TEXT,
                        'default' => esc_html__( 'Customer Name', 'wpkit-elementor' ),
                        'show_label' => false,
                    ],
                    [
                        'name' => 'job',
                        'label' => esc_html__( 'Job Position', 'wpkit-elementor' ),
                        'type' => Controls_Manager::TEXT,
                        'default' => esc_html__( 'Designer', 'wpkit-elementor' ),
                        'show_label' => false,
                    ],
                    [
                        'name' => 'avatar',
                        'label' => esc_html__( 'Customer Avatar', 'wpkit-elementor' ),
                        'type' => Controls_Manager::MEDIA,
                        'default' => [
                            'url' => Utils::get_placeholder_image_src(),
                        ],
                        'show_label' => false,
                    ],
                    [
                        'name' => 'said',
                        'label' => esc_html__( 'Content', 'wpkit-elementor' ),
                        'type' => Controls_Manager::TEXTAREA,
                        'default' => esc_html__( 'Slide Content', 'wpkit-elementor' ),
                        'show_label' => false,
                    ]
                ],
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->add_control(

            'text_color',
            [
                'type' => Controls_Manager::COLOR,
                'label' => esc_html__('Text Color', 'wpkit-elementor'),
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .testimonial-text' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .wke-testimonials .author' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .wke-testimonials .author em' => 'color: {{VALUE}}'
                ],
            ]
        );

        $this->add_control(

            'extra_class',
            [
                'type' => Controls_Manager::TEXT,
                'label' => esc_html__('Extra Class', 'wpkit-elementor'),
                'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'wpkit-elementor'),
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        WKE_Extend_Elementor::widget_template(self::get_name(), $this->get_settings());
    }

    protected function _content_template() {
        
    }

}
