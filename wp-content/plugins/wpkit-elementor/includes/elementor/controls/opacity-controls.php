<?php

/**
 * Opacity controls
 */

function wke_add_opacity_control_to_widget( $element, $section_id, $args ) {

    if ( 'common' === $element->get_name() && '_section_style' === $section_id ) {

        $element->start_controls_section(
            '_section_opacity',
            [
                'label' => esc_html__( 'Opacity', 'wpkit-elementor' ),
                'tab' => \Elementor\Controls_Manager::TAB_ADVANCED,
            ]
        );


        $element->add_control(
            'opacity',
            [
                'label' => esc_html__( 'Opacity', 'wpkit-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'description' => esc_html__('Generally, the value should between 0 and 1','wpkit-elementor'),
                'default' =>'1'
            ]
        );


        $element->end_controls_section();

    }

}
add_action( 'elementor/element/after_section_end', 'wke_add_opacity_control_to_widget', 10, 3 );

function wke_add_opacity_control_attributes_to_elements( \Elementor\Element_Base $element ) {
    if ( ! $element->get_settings( 'opacity' ) == '1' ) {
        return;
    }
    
    $element->add_render_attribute( '_wrapper', [
        'style' => 'opacity:'.$element->get_settings( 'opacity' )
    ] );
}
add_action( 'elementor/frontend/element/before_render', 'wke_add_opacity_control_attributes_to_elements' );
add_action( 'elementor/frontend/widget/before_render', 'wke_add_opacity_control_attributes_to_elements' );